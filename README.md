# PhoneMeWhen #

This is just a weird mockup in SVG of an app idea.  Does it run on a phone?  Is it a website?  Don't know yet.  It is mostly an experiment with doing mockups in SVG.

## The SVG Mockup itself (right click; open in new tab to test)
![the svg mockup](./mockup_01.svg)


## Same in PNG format (does not work as mockup)
![the png version](./mockup_01.png)
